#ifndef TESTWIN_H
#define TESTWIN_H

#include <QDialog>
#include <QDataWidgetMapper>

namespace Ui {
class Testwin;
}

class Testwin : public QDialog
{
    Q_OBJECT

public:
    explicit Testwin(QWidget *parent = nullptr);
    ~Testwin();
QDataWidgetMapper *qwerty;
void setModel(QAbstractItemModel*mops);
private slots:
void on_pushButton_clicked();

void on_pushButton_2_clicked();

private:
    Ui::Testwin *ui;
};

#endif // TESTWIN_H
