#ifndef GENERALWIN_H
#define GENERALWIN_H
#include "nomoney.h"
#include "historyone.h"
#include "statistic.h"
#include "sendmoney.h"

#include <QDialog>
#include <QSqlTableModel>
namespace Ui {
class generalWin;
}

class generalWin : public QDialog
{
    Q_OBJECT

public:
    explicit generalWin(QWidget *parent = nullptr);
    ~generalWin();

private slots:
    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();


     void on_static_2_clicked();

     void on_pushButton_2_clicked();



     void on_pushButton_clicked();

private:
    Ui::generalWin *ui;
    sendmoney *window6;
  generalWin *window5;
  statistic *window7;
  historyOne *window8;
  Nomoney *window10;
  QSqlTableModel *model3;
  QSqlTableModel *model4;
  QSqlDatabase *famaly;
  QSqlDatabase *famalyDohod;

};

#endif // GENERALWIN_H
