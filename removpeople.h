#ifndef REMOVPEOPLE_H
#define REMOVPEOPLE_H

#include <QDialog>

namespace Ui {
class removPeople;
}

class removPeople : public QDialog
{
    Q_OBJECT

public:
    explicit removPeople(QWidget *parent = nullptr);
    ~removPeople();

private:
    Ui::removPeople *ui;
};

#endif // REMOVPEOPLE_H
