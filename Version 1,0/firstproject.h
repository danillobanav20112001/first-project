#ifndef FIRSTPROJECT_H
#define FIRSTPROJECT_H
#include "addpeople.h"
#include "sendfamal.h"
#include "removpeople.h"
#include "generalwin.h"
#include <QWidget>
#include <QtSql>
#include <QtDebug>
#include <QFileInfo>
#include "QSqlDatabase"
#include "QSqlQuery"
#include <QSqlTableModel>
QT_BEGIN_NAMESPACE
namespace Ui { class FirstProject; }
QT_END_NAMESPACE

class FirstProject : public QWidget
{
    Q_OBJECT

public:
    FirstProject(QWidget *parent = nullptr);
    ~FirstProject();


private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_clicked();



private:
    Ui::FirstProject *ui;
    SendFamal *window;
    addPeople *window3;
    removPeople *window4;
    generalWin *window5;
    QSqlDatabase *famaly;
     QSqlDatabase *famaly2;
    QSqlTableModel *model;
    QSqlTableModel *model2;
};
#endif // FIRSTPROJECT_H
